<?php
// app/code/local/Joshua/Test/Model/Paymentmethod.php
class Joshua_Test_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {

    protected $_code  = 'test';

    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('test/paysafe/redirect', array('_secure' => false));
    }
}
