<?php

class Joshua_Test_PaysafeController extends Mage_Core_Controller_Front_Action
{
    public function redirectAction()
    {
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 'test', array('template' => 'joshua/api.phtml'));
        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }

    public function requestAction()
    {
        $amount = Mage::getSingleton('checkout/session')->getAmount();
        $amount = intval($amount) * 100;

        //get order
        $orderId = Mage::getSingleton('checkout/session')->getOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        //create unique merchant reference number
        $merchantRefNum = "joshuatest".$orderId;

        //get $_POST['token']
        $token = Mage::app()->getRequest()->getParam('token');

        //retrieve the values from session variables
        $accountId = Mage::getStoreConfig('test_section/test/account_id'); //account id
        $user = Mage::getStoreConfig('test_section/test/user'); //user
        $password = Mage::getStoreConfig('test_section/test/password'); //password

        $url = "https://api.test.paysafe.com/cardpayments/v1/accounts/".$accountId."/auths";
        $data = array(
                "merchantRefNum" => $merchantRefNum,
                "amount" => $amount,
                "settleWithAuth" => true,
                "card" => array(
                        "paymentToken" => $token
                    ),
                "billingDetails" => array(
                        "street" => $order->getBillingAddress()->getData('street'),
                        "city" => $order->getBillingAddress()->getCity(),
                        "state" => $order->getBillingAddress()->getRegion(),
                        "country" => $order->getBillingAddress()->getCountry(),
                        "zip" => $order->getBillingAddress()->getPostcode()
                    )
            );

        $data = json_encode($data);

        $http = new Varien_Http_Adapter_Curl();
        $http->setConfig(array('timeout' => 100, 'userpwd' => $user.":".$password));
        $http->write(Zend_Http_Client::POST, $url, CURL_HTTP_VERSION_1_0, array('Content-Type: application/json'), $data);
        $responseCheckOutData = Zend_Http_Response::extractBody($http->read());
        $http->close();

        $responseCheckOutData = json_decode($responseCheckOutData);

        if ($responseCheckOutData->status == "COMPLETED") {
            $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true, 'Payment Success.');
            $order->save();

            Mage::getSingleton('checkout/session')->unsQuoteId();
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
        }
        elseif ($responseCheckOutData->status == "RECEIVED") {
            $order->setState(Mage_Sales_Model_Order::STATE_STATE_NEW, true, 'Our system has received the request and is waiting for the downstream processor’s response.');
            $order->save();

            Mage::getSingleton('checkout/session')->unsQuoteId();
            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
            Mage::getSingleton('core/session')->addNotice('Our system has received the request and is waiting for the downstream processor’s response.');
        }
        elseif ($responseCheckOutData->status == "HELD") {
            $order->setState(Mage_Sales_Model_Order::STATE_HOLDED, true, 'The transaction has been placed on hold due to risk considerations.');
            $order->save();

            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=> false));
            Mage::getSingleton('core/session')->addError($responseCheckOutData->error->message);
        }
        elseif ($responseCheckOutData->status == "CANCELLED") {
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, ' The request has been fully reversed.');
            $order->save();

            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=> false));
            Mage::getSingleton('core/session')->addError($responseCheckOutData->error->message);
        }
        else {
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, ' The request has been fully reversed.');
            $order->save();

            Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/failure', array('_secure'=> false));
            Mage::getSingleton('core/session')->addError($responseCheckOutData->error->message);
        }
    }
}
